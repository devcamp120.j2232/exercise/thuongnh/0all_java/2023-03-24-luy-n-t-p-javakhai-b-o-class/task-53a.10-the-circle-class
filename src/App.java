import models.*;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle();
        Circle circle2 = new Circle(3.0);
        //circle.setRadius(105);
        System.out.println("hình tròn 1");
        System.out.println(circle.getRadius());
        System.out.println(circle.getArena());
        System.out.println(circle.getCircumference());
        System.out.println(circle.toString());


        System.out.println("hình tròn 2");
        System.out.println(circle2.getRadius());
        System.out.println(circle2.getArena());
        System.out.println(circle2.getCircumference());
        System.out.println(circle2.toString());

    }
}
